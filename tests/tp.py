# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return (n * "S") + "0"


def S(n: str) -> str:
    return "S" + n


def addition(a: str, b: str) -> str:
    return a[:-1] + b


def multiplication(a: str, b: str) -> str:
    return (a[:-1].count("S") * b[:-1].count("S")) * "S" + "0"


def facto_ite(n: int) -> int:
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result


def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    return n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    if n == 1:
        return 1
    return fibo_rec(n - 1) + fibo_rec(n - 2)


def fibo_ite(n: int) -> int:
    a = 0
    b = 1
    for _ in range(n):
        a, b = b, a + b
    return a


def golden_phi(n: int) -> int:
    if n == 1:
        return 1
    elif n == 2:
        return 2

    a, b = 1, 1
    for _ in range(n - 1):
        a, b = b, a + b

    return b / a


def sqrt5(n):
    phi = golden_phi(n+1)
    return 2 * phi - 1



def pow(a: float, n: int) -> float:
    return a ** n
